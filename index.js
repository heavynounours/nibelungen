const express = require('express')
const app = express()
const http = require('http').Server(app)
const geoip = require('geoip-lite');
const Tail = require('tail').Tail;
const io = require('socket.io')(http);
const fs = require('fs');
const countryIso = require('country-iso');
const path = require('path');
var moment = require('moment-strftime');
var LineByLineReader = require('line-by-line');

let ips = {};
let files = [];
let dates = [];

// geopip-lite update 
//node ./node_modules/geoip-lite/scripts/updatedb.js

//console argument
var arg_path = process.argv.slice(2).toString();

// static folder
app.use(express.static('public'));

// line parser
const source = '$http_client_ip - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$upstream_response_time" $request_time $host $upstream_status $upstream_addr $http_deviceType $http_productId $http_appVersion $http_market';
const parser = require("nginx-log-parser")(source);

//getting file list in folder & update when changes & get file last modification date
function getDates(files) {
    let dates = [];
    for (var i = 0; i < files.length; i++) {
        var stats = fs.statSync(arg_path + '/' + files[i]);
        var date = new Date(stats.mtime);
        date = moment(date).startOf('hour').fromNow();
        dates[i] = date;
    }
    return dates;
}

function listFiles() {
    fs.readdir(arg_path, function (err, items) {
        files = items;
        dates = getDates(files);
    });
}

fs.watch(arg_path, (eventType, filename) => {
    listFiles();
});
listFiles();

function parseLine(data) {
    data = parser(data)
    if (typeof ips[data.http_client_ip] === 'undefined') {
        var geo = geoip.lookup(data.http_client_ip);
        if (geo === null) {
            return {};
        }
        ips[data.http_client_ip] = {
            ip: data.http_client_ip,
            latitude: geo.ll[0],
            longitude: geo.ll[1],
            pays: countryIso.get(geo.ll[0], geo.ll[1]),
            passages: [],
            requetes: [],
            agents: [],
            status: []
        };
        io.emit('new-marker', {
            ip: data.http_client_ip,
            latitude: geo.ll[0],
            longitude: geo.ll[1],
            pays: countryIso.get(geo.ll[0], geo.ll[1]),
            passages: [],
            requetes: [],
            agents: [],
            status: []
        });
    }

    var time_local = data.time_local.replace('[','').replace(']','');
    time_local = moment(time_local,'DD/MMM/YYYY:HH:mm:ss ZZ').format('llll');;

    ips[data.http_client_ip].passages.push(time_local);
    ips[data.http_client_ip].requetes.push(data.request);
    ips[data.http_client_ip].agents.push(data.http_user_agent);
    ips[data.http_client_ip].status.push(data.status);
}

// file stream
let default_file = 'access.log'; // default file is access.log
tail = new Tail(arg_path + '/' + default_file, { fromBeginning: true });
tail.on("line", parseLine);
tail.on("error", function (error) { console.log('ERROR: ', error); });
// --------------------------

// reverse DNS


// web server
app.get('/', function (req, res) {
    res.sendFile("public/map.html", {
        root: __dirname
    });
})

// web socket in/out
io.on('connection', socket => {
    socket.emit('full-data', ips); // data marker send
    socket.emit('files', { files: files, dates: dates }); // sending files list
    socket.on('file', function (request_file) {
        let lr = new LineByLineReader(`${ arg_path }/${ request_file }`);
        let dataToSend = {};
        lr.on("line", (data) => {
            data = parser(data)
            if (typeof dataToSend[data.http_client_ip] === 'undefined') {
                var geo = geoip.lookup(data.http_client_ip);
                if (geo === null) {
                    return {};
                }
                dataToSend[data.http_client_ip] = {
                    ip: data.http_client_ip,
                    latitude: geo.ll[0],
                    longitude: geo.ll[1],
                    pays: countryIso.get(geo.ll[0], geo.ll[1]),
                    passages: [],
                    requetes: [],
                    agents: [],
                    status: []
                };
            }

            var time_local = data.time_local.replace('[','').replace(']','');
            time_local = moment(time_local,'DD/MMM/YYYY:HH:mm:ss ZZ').format('llll');;

            dataToSend[data.http_client_ip].passages.push(time_local);
            dataToSend[data.http_client_ip].requetes.push(data.request);
            dataToSend[data.http_client_ip].agents.push(data.http_user_agent);
            dataToSend[data.http_client_ip].status.push(data.status);
        });
        lr.on("end", () => {
            socket.emit('full-data', dataToSend);
        });
    });
})

http.listen(3000, function () {
    console.log('Nibelungen - Log parser & IP Tracer -  running on port 3000')
})